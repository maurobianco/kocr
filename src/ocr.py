import cv2
import numpy as np
import sys

class wherewho:
    def __init__(self, px, py, ch):
        self.px = px
        self.py = py
        self.ch = ch

    def __repr__(self):
        return '<' + str(self.px) + ', ' + str(self.py) + ', ' + chr(self.ch+48) + '>'

def sum(a):
    sum = 0
    for i in range(a.shape[0]):
        for j in range(a.shape[1]):
            sum = sum + a[i][j]*a[i][j]
    return sum

def check_pattern(pattern, img, I,J):
    m=np.zeros((pattern.shape[0],pattern.shape[1]))
    for i in range(1,pattern.shape[0]-1):
        for j in range(1,pattern.shape[1]-1):
            m[i][j]=pattern[i][j]-img[i+I][j+J]
            
    return sum(m)

def enhance_img(img):
    newi = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i][j] > 60:
                newi[i][j]=255
    return newi

def enhance_patterns(patterns):
    newps=list()
    print("%d\n" % len(patterns))
    for lp in patterns:
    #    print(str(patterns))
        innerlist=list()
        print("len %d\n" % len(lp))
        print("the WHOLE thing %r\n" % (lp))
        for p in lp: # Same symbol multiple versions
    #        print(str(p))
            print("the thing %r\n" % (p))
            newp = np.zeros((p.shape[0], p.shape[1]), dtype=np.uint8)
            for i in range(newp.shape[0]):
                for j in range(newp.shape[1]):
                    if p[i][j] > 60:
                        newp[i][j]=255
            innerlist.append(newp)
        newps.append(innerlist)
    print("Res len %d" % (len(newps)))
    for p in newps:
        print("Res inner len %d" % (len(p)))
            
    return newps

def loadpatterns(path, ext):
    #print("loading %s" % (path+'/0'+ext))
    img = list()
    name=path+'/0.'+ext
    print(name)
    
    img.append([ cv2.imread(name,0) ])
    img.append([cv2.imread((path+'/1.'+ext),0)])
    img.append([cv2.imread((path+'/2.'+ext),0)])
    img.append([  cv2.imread(path+'/3.3.'+ext,0), cv2.imread(path+'/3.'+ext,0), cv2.imread(path+'/3.2.'+ext,0), cv2.imread(path+'/3.4.'+ext,0)  ])
    img.append([cv2.imread((path+'/4.'+ext),0)])
    img.append([cv2.imread((path+'/5.'+ext),0)])
    img.append([cv2.imread((path+'/6.'+ext),0), cv2.imread((path+'/6.2.'+ext),0)])
    img.append([cv2.imread((path+'/7.'+ext),0)]) #, cv2.imread(path+'/7.2.'+ext,0)] )
    img.append([cv2.imread((path+'/8.'+ext),0), cv2.imread((path+'/8.2.'+ext),0)])
    img.append([cv2.imread((path+'/9.'+ext),0)])
    img.append([cv2.imread((path+'/Pipe.'+ext),0)])
    img.append([cv2.imread((path+'/PipeMinus.'+ext),0)])
    img.append([cv2.imread((path+'/MinusPipe.'+ext),0)])
    img.append([cv2.imread((path+'/Plus.'+ext),0)])
    return img

def autocorrelate(img):
    x=10
    y=10
    m=np.zeros((x,y))
    for i in range(x):
        for j in range(j):
            m[i][j] = sum(img[i]-img[j])
    return m

def go(pattern, immagine):
    n=immagine.shape[0]
    pn=pattern.shape[0]
    m=immagine.shape[1]
    pm=pattern.shape[1]
    res=np.zeros((n-pn,m-pm))
    for i in range(n-pn):
        for j in range(m-pm):
            res[i][j]=check_pattern(pattern, immagine, i,j)
            
    #resi=(res-np.amin(res)) / (np.amax(res)-np.amin(res)) *255
    return res

def show(name, img):
    ratio = float(img.shape[0])/float(img.shape[1])
    imgscale = cv2.resize(img, (int(1000/ratio), 1000)) 
    cv2.imshow(name, imgscale)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    cv2.waitKey(0)

def match_and_show(pattern, img):
    res = cv2.matchTemplate(img, pattern, cv2.TM_SQDIFF_NORMED)
    threshold = 0.95
    loc = np.where( res <= (1-threshold))
    w, h = pattern.shape[::-1]
    for pt in zip(*loc[::-1]):
        cv2.rectangle(img, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
    show('immagine', img)

def match_and_show_all(patterns, img):
    for i in patterns:
        res = cv2.matchTemplate(img, patterns[i], cv2.TM_SQDIFF_NORMED)
        threshold = 0.94
        loc = np.where( res <= (1-threshold))
        w, h = patterns[i].shape[::-1]
        for pt in zip(*loc[::-1]):
            cv2.rectangle(img, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)

    show('immagine', img)

def match_all(patterns, img):
    res = list()
    pt=0
    im=0
    print('Patterns %d' % (len(patterns)))
    for l in patterns:
        print('Num imahges %d' % (len(l)))
        inner=list()
        for i in l:
            print("Matching pattern %d (img %d)" % (pt,im))
            inner.append(cv2.matchTemplate(img, i, cv2.TM_SQDIFF_NORMED))
            im=im+1
        res.append(inner)
        pt=pt+1
    return res

def show_all(patterns, img, results, threshold):
    image=img
    w, h = patterns[0].shape[::-1]
    for i in results:
        loc = np.where( results[i] <= (1-threshold))
        for pt in zip(*loc[::-1]):
            cv2.rectangle(image, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)

    show('immagine', image)
    return loc

def merge_results(res):
    threshold=np.ones(res[0][0].shape)
    symbol=255*np.ones(res[0][0].shape, dtype=np.uint8)
    for i in range(11):
        for j in range(len(res[i])):
            print('%d x %d' % (res[i][j].shape[0], res[i][j].shape[1]))
            for pti in range(res[i][j].shape[0]):
                for ptj in range(res[i][j].shape[1]):
                    if threshold[pti][ptj] >= res[i][j][pti][ptj]:
                        threshold[pti][ptj] = res[i][j][pti][ptj]
                        symbol[pti][ptj] = i
                    
    return threshold, symbol

def decluster(ths, symbol, threshold, neigh=3):
    newths = np.ones(ths.shape)
    newsym = np.zeros(symbol.shape, dtype=np.uint8)
    for pti in range(ths.shape[0]):
        for ptj in range(ths.shape[1]):
            if ths[pti][ptj] < threshold:
                # look for neighbors
                val = ths[pti][ptj]
                posi = pti
                posj = ptj
                sym = symbol[pti][ptj]
                for i in range(-neigh, neigh):
                    for j in range(-neigh, neigh):
                        if pti+i > 0 and ptj+j > 0 and pti+i < ths.shape[0] and ptj+j < ths.shape[1]:
                            if ths[pti+i][ptj+j]<val :
                                val = ths[pti+i][ptj+j]
                                posi=pti+i
                                posj=ptj+j
                                sym = symbol[pti+i][ptj+j]
                if posi==pti and posj==ptj:
                    newsym[pti][ptj] = sym
                    newths[pti][ptj] = val
    print ('Phase I Done')

    return newths, newsym

def enlist(newths, newsym, threshold):
    places=np.where(newths < threshold)
    inlist = []
    for i in range(places[0].shape[0]):
        if newsym[places[0][i]][places[1][i]] < 10:
            inlist = inlist + [wherewho(places[0][i], places[1][i], newsym[places[0][i]][places[1][i]])]
    print ('Phase II Done: char found %d' % (len(inlist)))
    return inlist

def arrange(inlist):
    lst = sorted(inlist, key=lambda x : x.px)
    output=[]
    # findingh the chunk at more or less the same y position
    start = 0
    while start<len(lst):
        end=start+1
        pos = lst[start].px
        i=1
        while end<len(lst) and abs(lst[end].px-pos)<20 :
            end=end+1
        output += [sorted(lst[start:end], key=lambda x: x.py)]
        start=end
        print('start=%d len=%d end=%d' % (start, len(lst), end))
    return output
    
def print_chars(lstlst, h, w):
    screen = np.ones((400,200), dtype=np.uint8)*32
    for i in range(len(lstlst)):
        for j in lstlst[i]:
            #print i , j.px, int(np.floor(j.py/float(w/2)))
            ni = i
            nj = int(np.floor(j.py/float(w)))
            
            while screen[ni][nj] != 32:
                nj=nj+1
            screen[ni][nj] = j.ch+48
            
    for i in range(screen.shape[0]):
        for j in range(screen.shape[1]):
            sys.stdout.write( '%s' % chr(screen[i][j]) )
        print
    return screen
    
def bad_print_chars(lstlst, h, w):
    screen = np.ones((400,200), dtype=np.uint8)*32
    for i in range(len(lstlst)):
        pos = 0
        for j in lstlst[i]:
            screen[i][pos] = j.ch+48
            pos=pos+1
            
    for i in range(screen.shape[0]):
        for j in range(screen.shape[1]):
            sys.stdout.write( '%s' % chr(screen[i][j]) )
        print
    return screen
    
def load1200():
    img=loadpatterns('/Users/mbianco/Work/kocr/patterns/1200', 'jpg')
    times = cv2.imread('/Users/mbianco/Work/kocr/imgs/KartingCrop1200.jpg',0)
    return img, times

def flow1200():
    img, times = load1200()
    res = match_all(img, times)
    th, char = merge_results(res)
    nth, nsym, inlst = decluster(th, char, 0.1)
    #screen=print_chars(th, char, 0.1, img[0].shape[0], img[0].shape[1])
    lst = arrange(inlst)
    return img, times, res, th, char, nth, nsym, inlst, lst

def load600(input):
    img=loadpatterns('/Users/mbianco/Work/kocr/patterns/600', 'jpg')
    #print('Loading %s', '/Users/mbianco/Work/kocr/imgs/' + input)
    times = cv2.imread('/Users/mbianco/Work/kocr/imgs/' + input,0)
    return img, times

def flow600(input):
    print("Loading")
    dimg, dtimes = load600(input)
    print("Inverting patterns")
    img=enhance_patterns(dimg)
    print("Inverting image")
    times = enhance_img(dtimes)
    for i in img:
        for j in i:
            for k in img:
                for l in k:
                    print('%r\t' % cv2.matchTemplate(j, l, cv2.TM_SQDIFF_NORMED)[0][0])
            print(" ")
                    

    print("Mathcing")
    res = match_all(img, times)
    print("Merging")
    th, char = merge_results(res)
    print("Decluster")
    nth, nsym= decluster(th, char, 0.09,15)
    print("Enlistigg")
    inlst = enlist(nth, nsym, 0.09)
    #screen=print_chars(th, char, 0.1, img[0].shape[0], img[0].shape[1])
    print("Listing")
    lst = arrange(inlst)
    return img, times, res, th, char, nth, nsym, inlst, lst
